<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SmsRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Sms\SmsRepositoryInterface',
            'App\Repositories\Sms\SmsRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
