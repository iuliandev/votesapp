<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ContactsRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Contacts\ContactsRepositoryInterface',
            'App\Repositories\Contacts\ContactsRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
