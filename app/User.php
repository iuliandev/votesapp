<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\MultipleNumbers;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    const STATUS_UNREGISTERED = 'Unregistered';
    const STATUS_REGISTERED = 'Registered';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone_number',
        'zipcode',
        'city',
        'status',
        'avatar'
    ];

    /**
     * Create user
     *
     * @param array $fields
     *
     * @return $this
     */
    public function createIfNotExist($user)
    {
        $user_exist = $this->where('phone_number', $user['phone_number'])
                           ->first();

        if(!$user_exist) {
            $in_contacts = MultipleNumbers::where('phone_number', $user['phone_number'])
                                          ->first();

            if(!$in_contacts) {
                $this->create($user);
            }
        }

        return $this;
    }

    /**
     * Find user by phone
     *
     * @param string $phone_number
     *
     * @return $this
     */
    public function findByPhone($phone_number)
    {
        $user = $this->where('phone_number', $phone_number)
                     ->first();

        if(!$user) {
            $in_contacts = MultipleNumbers::where('phone_number', $phone_number)
                                          ->first();
            if($in_contacts) {
                $user = $in_contacts->user;
            }
        }

        return $user;
    }

    /**
     * Get user contacts
     *
     * @return $this
     */
    public function contacts()
    {
        return $this->belongsToMany('App\User', 'user_contacts', 'user_id', 'contact_id')
                    ->withPivot(['id', 'contact_name'])
                    ->withTimestamps();
    }

    public function leader()
    {
        return $this->belongsToMany('App\User', 'user_contacts', 'contact_id', 'user_id')
                    ->withTimestamps();
    }

    /**
     * Get user related numbers from contacts where he is in
     *
     * @return $this
     */
    public function related_numbers()
    {
        return $this->belongsTo('App\MultipleNumbers');
    }
}
