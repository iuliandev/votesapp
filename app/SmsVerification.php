<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsVerification extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone_number',
        'code',
        'attemps',
        'status'
    ];

    /**
     * Store sms confirmation data
     *
     * @param Request $request
     *
     * @return $this
     */
    public function store($request)
    {
        $this->fill($request->all());
        $this->save();

        return $this;
    }

    /**
     * Update sms confirmation data
     *
     * @param Request $request
     *
     * @return $this
     */
    public function updateModel($request)
    {
        $this->update($request->all());

        return $this;
    }

    /**
     * Update entity status
     *
     * @param string $status
     *
     * @return $this
     */
    public function updateStatus($status)
    {
        $this->status = $status;
        $this->save();

        return $this;
    }

    /**
     * Get confirmed
     *
     * @param Request $request
     *
     * @return $this
     */
    public function getConfirmedVerification($request)
    {
        return $this->where('phone_number', $request->phone_number)
                    ->where('code', $request->code)
                    ->where('status', 'confirmed')
                    ->first();
    }

    /**
     * Get entity by phone and code
     *
     * @param Request $request
     *
     * @return $this
     */
    public function getEntity($request)
    {
        return $this->where('phone_number', $request->phone_number)
                    ->where('code', $request->code)
                    ->first();
    }

    /**
     * Get latest entity by phone
     *
     * @param Request $request
     *
     * @return $this
     */
    public function getLatestVerification($request)
    {
        return $this->where('phone_number', $request->phone_number)
                    ->latest()
                    ->first();
    }

    /**
     * Update old entities status
     *
     * @param int $exclude_entity_id
     *
     * @return $this
     */
    public function updateEntitiesStatus($exclude_entity_id)
    {
        return $this->whereNotIn('id', [$exclude_entity_id])
                    ->update(array('status' => 'expired'));
    }

}
