<?php

namespace App\Http\Middleware;

use App\SmsVerification;
use DateTime;
use Closure;

class CheckIsSmsConfirmed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $sms_verifcation = new SmsVerification();
        $entity = $sms_verifcation->getConfirmedVerification($request);


        if(!$entity) {
            return jsonResponse('error', 401, [
                'message' => getError('not_confirmed')
            ]);
        }

        $date = new DateTime();
        $date->modify('-24 hours');
        $current_date = $date->format('Y-m-d H:i:s');
        $expired = (bool)($current_date > $entity->created_at);

        if($expired) {
            return jsonResponse('error', 401, [
                'message' => getError('expired')
            ]);
        }

        return $next($request);
    }
}
