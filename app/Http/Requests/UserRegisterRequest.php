<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'        => 'required|string|email',
            'name'         => 'required|string',
            'zipcode'      => 'required|postal_code:US',
            'phone_number' => 'phone:US,MD,mobile',
            // 'avatar'       => 'nullable|mimes:jpeg,jpg,png,gif|max:100000'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'zipcode.postal_code' => 'The :attribute should be US.',
            'phone_number.phone' => 'The :attribute is an invalid mobile number.',
        ];
    }
}
