<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VerifyContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|numeric|min:10000|max:99999',
            'phone_number' => 'phone:US,MD,mobile'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'code.numeric' => 'The code must be a number from 5 digits.',
            'code.min' => 'The code must be a number from 5 digits.',
            'code.max' => 'The code must be a number from 5 digits.',
            'phone_number.phone' => 'The :attribute is an invalid mobile number.',
        ];
    }
}
