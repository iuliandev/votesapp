<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SendSmsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'phone_number' => 'phone:US,mobile'
            'phone_number' => 'phone:US,MD,mobile'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'phone_number.phone' => 'The :attribute is an invalid mobile number.',
        ];
    }
}
