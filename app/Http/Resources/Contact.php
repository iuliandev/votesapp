<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\NumbersCollection;
use App\UserContacts;

class Contact extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $contact = UserContacts::where('user_id', auth()->user()->id)
                             ->where('contact_id', $this->id)
                             ->first();

        $multiple_numbers = array();

        if($contact->multiple_numbers) {
            $multiple_numbers = $contact->multiple_numbers
                                        ->where('phone_number', '<>', $this->phone_number);
        }

        return [
            'id'              => $this->id,
            'name'            => $this->pivot->contact_name,
            'email'           => $this->email,
            'phone_number'    => $this->phone_number,
            'zipcode'         => $this->zipcode,
            'city'            => $this->city,
            'status'          => $this->status,
            'avatar'          => mediaFile($this->avatar),
            'created_at'      => $this->created_at->diffForHumans(),
            'related_numbers' => new NumbersCollection($multiple_numbers)
        ];
    }
}
