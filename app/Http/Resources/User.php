<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'              => $this->id,
            'name'            => $this->name,
            'email'           => $this->email,
            'phone_number'    => $this->phone_number,
            'zipcode'         => $this->zipcode,
            'city'            => $this->city,
            'status'          => $this->status,
            'avatar'          => mediaFile($this->avatar),
            'created_at'      => $this->created_at->diffForHumans()
        ];
    }
}
