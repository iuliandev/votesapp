<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class AppController extends Controller
{
    public $defaultErrorMessage = 'Something went wrong! Please try again or contact us to report it.';

    public function failMessage()
    {
      return response([
          'message' => $this->defaultErrorMessage
      ], 500);
    }
}
