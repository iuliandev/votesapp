<?php

namespace App\Http\Controllers\Api;

use App\Repositories\Contacts\ContactsRepositoryInterface;
use App\Http\Controllers\AppController;
use Illuminate\Http\Request;
use App\Http\Resources\ContactCollection;
use App\Http\Resources\Contact as ContactResource;

class ContactsController extends AppController
{
    /**
     * Contacts repository
     */
    private $contactsRepository;

    /**
     * Constructor
     *
     * @param ContactsRepositoryInterface $contactsRepository
     */
    public function __construct(ContactsRepositoryInterface $contactsRepository)
    {
        $this->contactsRepository = $contactsRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $contacts = $this->contactsRepository
                             ->getUserContacts();

            return jsonResponse('success', 201, new ContactCollection($contacts));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Synchronize user contacts
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sync(Request $request)
    {
        try {
            $contacts = array();
            $filtered_contacts = $this->contactsRepository
                                      ->filterContacts($request);

            $this->contactsRepository
                 ->storeContacts($filtered_contacts);

            $contacts = $this->contactsRepository
                             ->getUserContacts();

            return jsonResponse('success', 201, new ContactCollection($contacts));
        } catch(\Exception $e) {
            return $e->getMessage();
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Display the specified user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $contact = null;

            $contact = $this->contactsRepository
                            ->getContact($id);

            if(!$contact)
                return jsonResponse('error', 404, [
                    'message' => 'Not found'
                ]);

            return jsonResponse('success', 201, new ContactResource($contact));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Remove the specified contact from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->contactsRepository
                 ->removeContacts(array($id));

            return jsonResponse('success', 200, []);
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Remove the multiple contacts from storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroyMultiple(Request $request)
    {
        try {
            if(count($request->contacts) < 1)
                return jsonResponse('error', 401, [
                    'message' => 'No selected items.'
                ]);

            $this->contactsRepository
                 ->removeContacts($request->contacts);

            return jsonResponse('success', 200, []);
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }
}
