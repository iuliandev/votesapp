<?php

namespace App\Http\Controllers\Api;

use App\Repositories\User\UserRepositoryInterface;
use App\Http\Requests\UserRegisterRequest;
use App\Http\Controllers\AppController;
use App\Http\Resources\User as UserResource;
use Illuminate\Http\Request;
use App\User;

class UserController extends AppController
{
    /**
     * User repository
     */
    private $userRepository;

    /**
     * Constructor
     *
     * @param UserRepositoryInterface $userRepository User repository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Create new user
     *
     * @param UserRegisterRequest $request
     *
     * @return array
     */
    public function store(UserRegisterRequest $request) {
        try {
            $response = array();

            $user = $this->userRepository
                         ->createOrUpdate($request);

            $this->userRepository
                 ->setTokenExpiration();

            $response['user'] = new UserResource($user);
            $response['access_token'] = $this->userRepository
                                             ->getToken($user);

            return jsonResponse('success', 201, $response);
        } catch(\Exception $e) {
            return $e->getMessage();
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Display auth user resource.
     *
     * @return UserResource
     */
     public function show()
     {
         try {
             $user = $this->userRepository
                          ->getAuthUser();

             if(!$user)
                 return jsonResponse('success', 200, []);

             return jsonResponse('success', 200, new UserResource($user));
         } catch(\Exception $e) {
             \Log::info(debugInfo($e));

             return $this->failMessage();
         }
     }
}
