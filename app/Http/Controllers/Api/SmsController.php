<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\AppController;
use Illuminate\Http\Request;
use App\Http\Requests\SendSmsRequest;
use App\Http\Requests\VerifyContactRequest;
use App\Repositories\Sms\SmsRepositoryInterface;
use Twilio\Exceptions\RestException;
use App\SmsVerification;

class SmsController extends AppController
{
    /**
     * Sms verification repository
     */
    protected $smsRepository;

    /**
     * Constructor
     *
     * @param SmsVerification $smsVerifcation Sms model
     */
    function __construct(SmsRepositoryInterface $smsRepository)
    {
        $this->smsRepository = $smsRepository;
    }

    /**
     * Store verification data and send confirmation sms
     *
     * @param  App\Http\Requests\SendSmsRequest  $request
     *
     * @return array
     */
    public function store(SendSmsRequest $request)
    {
        try {
            $this->smsRepository->create($request);
            $response = $this->smsRepository->sendSms($request);

            $sms_code = explode(" ", $response->body);

            if($response->sid) {
                return jsonResponse('success', 201, [
                    'message' => 'You will receive the verification code shortly.',
                    'sms_body' => $sms_code[1]
                ]);
            }

            return jsonResponse('error', 400, [
                'message' => 'Something went wrong. Please try again.'
            ]);
        } catch (RestException $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 400, [
                'message' => getError($e->getCode())
            ]);
        } catch (Exception $e) {
            \Log::info(debugInfo($e));

            return $this->failMessage();
        }
    }

    /**
     * Check confirmation code
     *
     * @param  App\Http\Requests\VerifyContactRequest  $request
     *
     * @return array
     */
    public function verifyContact(VerifyContactRequest $request)
    {
        try {
            $sms_availability = $this->smsRepository
                                     ->checkSmsAvailability($request);

            if(!$sms_availability['available']) {
                return jsonResponse('error', 401, [
                    'message' => $sms_availability['message']
                ]);
            }

            return jsonResponse('success', 200, []);
        } catch (Exception $e) {
            \Log::info(debugInfo($e));

            return $this->failMessage();
        }
    }
}
