<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\AppController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class MediaController extends AppController
{
  /**
   * Get media
   *
   * @param  string  $entity
   * @param  string  $folder
   * @param  string  $file
   *
   * @return string
   */
    public function index($entity, $folder, $file)
    {
        try {
            $filePath = storage_path('app/'.$entity.'/'.$folder.'/'.$file);

            if (!file_exists($filePath)) {
                return jsonResponse('error', 404, [
                    'message' => 'File doesn\'t exist.'
                ]);
            }

            return response()->file($filePath);
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return $this->failMessage();
        }
    }
}
