<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MultipleNumbers extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'phone_number'
    ];

    public $timestamps = false;

    /**
     * Get contact of number
     *
     * @return $this
     */
    public function contact()
    {
        return $this->belongsTo('App\UserContacts');
    }

    /**
     * Get user of number
     *
     * @return $this
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
