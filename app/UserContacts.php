<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserContacts extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'contact_id',
        'contact_name'
    ];

    public $timestamps = true;

    /**
     * Get user multiple numbers
     *
     * @return $this
     */
    public function multiple_numbers()
    {
        return $this->hasMany('App\MultipleNumbers', 'user_contact_id');
    }

    /**
     * Create users from contacts
     *
     */
    public function addUsersToContactList($newContacts)
    {
        // auth()->user()
        //       ->contacts()
        //       ->sync($newContacts);

        $user = auth()->user();
        $arr = array();

        foreach ($newContacts as $contact) {
            $exist_contact = $this->where('user_id', $user->id)
                                  ->where('contact_id', $contact['contact_id'])
                                  ->first();

            if($exist_contact) {
                array_push($arr, $exist_contact->id);
                $exist_contact->contact_name = $contact['contact_name'];
                $exist_contact->save();
            } else {
                $user->contacts()
                     ->attach(
                        array( 0 => $contact )
                     );

                $latest = $user->contacts()
                                ->latest()
                                ->first();

                array_push($arr, $latest->pivot_id);
            }
        }

        $this->where('user_id', $user->id)
             ->whereNotIn('id', $arr)
             ->delete();
    }

    /**
     * Add multiple numbers to contact
     *
     */
    public function addMultipleNumbers($multipleNumbers)
    {
        $user_id = auth()->user()->id;

        array_map( function($contact) use ($user_id) {
            if($contact['multiple_numbers']) {
                $created_contact = $this->where('user_id', $user_id)
                                        ->where('contact_id', $contact['contact_id'])
                                        ->first();

                $created_contact->multiple_numbers()->delete();

                $newContacts = array_map( function($number) use ($created_contact) {
                    $created_contact->multiple_numbers()->create([
                          'user_id' => $created_contact->contact_id,
                          'phone_number' => $number
                        ]
                    );
                }, $contact['multiple_numbers'] );
            }
        }, $multipleNumbers );
    }

    /**
     * Remove users from contacts
     *
     */
    public function removeUsersFromContactList($ids)
    {
         auth()->user()
               ->contacts()
               ->whereIn('contact_id', $ids)
               ->delete();
    }
}
