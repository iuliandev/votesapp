<?php

namespace App\Repositories\User;

use App\User;
use PragmaRX\ZipCode\ZipCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;
use Carbon\Carbon;
use App\MultipleNumbers;

class UserRepository implements UserRepositoryInterface
{
    /**
     * User model
     */
    private $user;

    /**
     * Constructor
     *
     * @param User $user User entity
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Create or update existent user
     *
     * @param Request $request
     *
     * @return User
     */
    public function createOrUpdate(Request $request)
    {
        $fields = $request->except('avatar');
        $user_exist = $this->user
                           ->findByPhone($request->phone_number);

        $fields['status'] = $this->user::STATUS_REGISTERED;
        $fields['city'] = $this->getCity($request->zipcode);

        if($user_exist) {
            MultipleNumbers::where('phone_number', $user_exist->phone_number)
                           ->update(['phone_number' => $request->phone_number]);

            $fields['avatar'] = $this->setAvatar($request, $user_exist);
            $user_exist->update($fields);

            return $user_exist;
        }

        $fields['avatar'] = $this->setAvatar($request, $this->user);
        $this->user->fill($fields);
        $this->user->save();

        return $this->user;
    }

    /**
     * Get user
     *
     * @return User
     */
     public function getAuthUser()
     {
         return auth()->user();
     }

    /**
     * Get user acces token
     *
     * @param User $user User
     *
     * @return string
     */
    public function getToken(User $user)
    {
        return $user->createToken('AppName')->accessToken;
    }

    /**
     * Set tokens expiration
     */
    public function setTokenExpiration()
    {
        Passport::tokensExpireIn(Carbon::now()->addYears(15));
        Passport::refreshTokensExpireIn(Carbon::now()->addYears(15));
    }

    /**
     * Set user avatar
     *
     * @param Request $request
     * @param User $user
     *
     * @return string
     */
    private function setAvatar($request, $user)
    {
        $avatar = '';
        $avatar_text = $request->input('avatar');
        $avatar_file = $request->file('avatar');

        if($avatar_text == null && $avatar_file == null) {
            $this->removeCurrentAvatar($user);
            $avatar = 'default/avatar.png';
        } else if($avatar_file) {
            $avatar = $this->updateAvatar($avatar_file, $user);
        } else if($avatar_text) {
            $avatar = $user->avatar;
        }

        return $avatar;
    }

    /**
     * Update user avatar
     *
     * @param $avatar
     * @param User $user
     *
     * @return string
     */
    private function updateAvatar($avatar, $user)
    {
        $file_name = $avatar->getClientOriginalName();
        $folder_name = time().mt_rand(1, 999999);

        $image = Storage::disk('local')->putFileAs(
            'users/'.$folder_name,
            $avatar,
            $file_name
        );

        if($image) {
            $this->removeCurrentAvatar($user);
        }

        $image_url = $folder_name . '/' . $file_name;

        return $image_url;
    }

    /**
     * Remove curent user avatar
     *
     * @param User $user
     *
     * @return void
     */
    private function removeCurrentAvatar($user)
    {
        $image = $user->avatar;

        if(!empty($image) && $image != 'default/avatar.png') {
            $folder = strtok($image, '/');
            Storage::deleteDirectory('users/' . $folder);
        }
    }

    /**
     * Get user city by zipcode
     *
     * @param $zipcode
     *
     * @return sring
     */
    private function getCity($zipcode)
    {
        $location = null;
        $z = new ZipCode;
        $z->setCountry('US');
        $result = $z->find($zipcode)->toArray();

        if($result['success']) {
            $result = reset($result['addresses']);
            $location = $result['place'] . ' ' .  $result['state_id'];
        }

        return $location;
    }

}
