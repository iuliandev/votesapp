<?php

namespace App\Repositories\User;

use App\User;
use Illuminate\Http\Request;

interface UserRepositoryInterface
{
    public function createOrUpdate(Request $request);
}
