<?php

namespace App\Repositories\Contacts;

use Propaganistas\LaravelPhone\PhoneNumber;
use Illuminate\Http\Request;
use App\UserContacts;
use App\User;

class ContactsRepository implements ContactsRepositoryInterface
{
    /**
     * User model
     */
    private $user;

    /**
     * User Contacts model
     */
    private $userContacts;

    /**
     * Constructor
     *
     * @param User $user
     * @param UserContacts $userContacts
     */
    public function __construct(
        User $user,
        UserContacts $userContacts
    )
    {
        $this->user = $user;
        $this->userContacts = $userContacts;
    }

    /**
     * Filter user contacts
     *
     * @param $request
     *
     * @return array
     */
    public function filterContacts($request)
    {
        $filtered = array_map( function($contact) {
            $valid_numbers = array();
            $number = false;

            if(is_array($contact['phone_number'])) {
                $number = $this->getFirstUsMobileNumber($contact['phone_number']);

                if($number) {
                    $numbers = $contact['phone_number'];
                    $valid_numbers = $this->getMultipleValidNumbers($numbers);
                }
            } else {
                $number = $this->filter($contact['phone_number']);
            }

            if($number) {
                $contact['phone_number'] = $number;
                $contact['name'] = $contact['name'] ?? $contact['phone_number'];
                $contact['status'] = $this->user::STATUS_UNREGISTERED;

                if(count($valid_numbers) > 0) {
                    $contact['multiple_numbers'] = $valid_numbers;
                }

                return $contact;
            }
        }, $request->contact );

        $filtered = array_filter($filtered);

        return $filtered;
    }

    /**
     * Store user contacts
     *
     * @param $contacts
     *
     * @return UserContacts
     */
    public function storeContacts($contacts)
    {
        $newContacts = array();
        $contactsWithMultipleNumbers = array();

        foreach ($contacts as $key => $contact) {
            $this->user->createIfNotExist($contact);
            $user = $this->user->findByPhone($contact['phone_number']);

            $newContacts[$key] = array(
                'contact_id' => $user->id,
                'contact_name' => $contact['name']
            );

            $contactsWithMultipleNumbers[$key] = $newContacts[$key];
            $contactsWithMultipleNumbers[$key]['multiple_numbers'] = $contact['multiple_numbers'] ?? array();
        }

        $this->userContacts->addUsersToContactList($newContacts);
        $this->userContacts->addMultipleNumbers($contactsWithMultipleNumbers);
    }

    /**
     * Get user contacts
     *
     * @return UserContacts
     */
    public function getUserContacts()
    {
        $contacts = auth()->user()
                          ->contacts()
                          ->orderBy('name', 'ASC')
                          ->get();

        return $contacts;
    }

    /**
     * Get user contact
     *
     * @return UserContacts
     */
    public function getContact($id)
    {
        $contact = auth()->user()
                         ->contacts()
                         ->find($id);

        return $contact;
    }

    /**
     * Remove user contacts
     *
     */
    public function removeContacts($ids)
    {
        $this->userContacts->removeUsersFromContactList($ids);
    }

    /**
     * Check if is US phone number
     *
     * @param $phone
     *
     * @return bool
     */
    private function isUS($phone)
    {
        try {
            $result = PhoneNumber::make($phone)->isOfCountry(['MD', 'US']);
        } catch(\Exception $e) {
            $result = false;
        }

        return $result;
    }

    /**
     * Check if is mobile phone number
     *
     * @param $phone
     *
     * @return bool
     */
    private function isMobile($phone)
    {
        try {
            $result = PhoneNumber::make($phone, ['MD', 'US'])->isOfType('mobile');
        } catch(\Exception $e) {
            $result = false;
        }

        return $result;
    }

    /**
     * Check if it is not my self number
     *
     * @param $phone
     *
     * @return bool
     */
    private function isNotMyself($phone)
    {
        $result = $phone != auth()->user()->phone_number ?? true;

        return $result;
    }

    /**
     * Get first US mobile number
     *
     * @param array
     *
     * @return bool
     */
    private function getFirstUsMobileNumber($numbers)
    {
        $validNumber = false;

        foreach ($numbers as $number) {
            $validNumber = $this->filter($number);

            if($validNumber)
                return $validNumber;
        }

        return $validNumber;
    }

    /**
     * Select valid numbers for multiple contact
     *
     * @param array
     *
     * @return bool
     */
    private function getMultipleValidNumbers($numbers)
    {
        $validNumbers = array();

        foreach ($numbers as $number) {
            $validNumber = $this->filter($number);

            if($validNumber) {
                array_push($validNumbers, $validNumber);
            }
        }

        return $validNumbers;
    }

    /**
     * Filter number
     *
     * @param string
     *
     * @return bool
     */
    private function filter($number)
    {
        $is_US = $this->isUS($number);
        $is_mobile = $this->isMobile($number);
        $is_not_myself = $this->isNotMyself($number);

        if($is_US && $is_mobile && $is_not_myself) {
            return $number;
        }

        return false;
    }
}
