<?php

namespace App\Repositories\Contacts;

use Illuminate\Http\Request;

interface ContactsRepositoryInterface
{
    public function filterContacts($request);

    public function storeContacts($contacts);
}
