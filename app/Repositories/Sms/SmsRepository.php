<?php

namespace App\Repositories\Sms;

use App\SmsVerification;
use Illuminate\Http\Request;
use Twilio\Exceptions\RestException;
use Twilio\Rest\Client;
use DateTime;

class SmsRepository implements SmsRepositoryInterface
{
    /**
     * Sms verification model
     */
    protected $sms_verifcation;

    /**
     * Local
     */
    protected $attempts,
              $available_code;

    /**
     * Twilio credentials
     */
    protected $account_sid,
              $auth_token,
              $phone_from;

    /**
     * Constructor
     *
     * @param SmsVerification $sms_verifcation Sms model
     */
    function __construct(SmsVerification $sms_verifcation)
    {
        $this->sms_verifcation = $sms_verifcation;
        $this->attempts = 3;
        $this->available_code = '-20 minutes';
        $this->account_sid = config('app.twilio')['TWILIO_ACCOUNT_SID'];
        $this->auth_token = config('app.twilio')['TWILIO_AUTH_TOKEN'];
        $this->phone_from = config('app.twilio')['TWILIO_PHONE_FROM'];
    }

    /**
     * Create sms verification entity
     *
     * @param $request
     *
     * @return User
     */
    public function create($request)
    {
        $code = rand(10000, 99999);
        $request['code'] = $code;

        $this->sms_verifcation->store($request);
    }

    /**
     * Send sms confirmation
     *
     * @param $request
     *
     * @return Client
     */
    public function sendSms($request)
    {
        $twilio = new Client($this->account_sid, $this->auth_token);

        $result = $twilio->messages
                         ->create($request->phone_number,
                             array(
                                 'from' => $this->phone_from,
                                 'body' => 'CODE: '. $request->code
                             )
                         );

        return $result;
    }

    /**
     * Check sms confirmation
     *
     * @param $request
     *
     * @return array
     */
    public function checkSmsAvailability($request)
    {
        $result = array('available' => true);

        if($this->isConfirmed($request)) {
            return $result;
        }

        if(!$this->exist($request)) {
            $result['available'] = false;
            $result['message'] = getError('not_exist');

            return $result;
        }

        if($this->isExpired($request) || !$this->isPending($request)) {
            $result['available'] = false;
            $result['message'] = getError('expired');

            return $result;
        }

        if($this->maxAttempts($request)) {
            $result['available'] = false;
            $result['message'] = getError('max_attempts');

            return $result;
        }

        if(!$this->isAvailable($request)) {
            $result['available'] = false;
            $result['message'] = getError('not_available');

            return $result;
        }

        return $result;
    }

    /**
     * Check if is confirmed
     *
     * @param $request
     *
     * @return bool
     */
    protected function isConfirmed($request)
    {
        $result = $this->sms_verifcation
                       ->getConfirmedVerification($request);

        $status = (bool)$result;

        return $status;
    }

    /**
     * Check if exist sms confirmation request
     *
     * @param $request
     *
     * @return bool
     */
    protected function exist($request)
    {
        $result = $this->sms_verifcation
                       ->getLatestVerification($request);

        $status = (bool)$result;

        return $status;
    }

    /**
     * Check if sms confirmation has expired
     *
     * @param $request
     *
     * @return bool
     */
    protected function isExpired($request)
    {
        $date = new DateTime;

        $date->modify($this->available_code);
        $current_date = $date->format('Y-m-d H:i:s');

        $entity = $this->sms_verifcation
                       ->getLatestVerification($request);

        $expired = (bool)($current_date > $entity->created_at);

        if($expired) {
            $entity->updateStatus('expired');
        }

        return $expired;
    }

    /**
     * Check if sms has expired status
     *
     * @param $request
     *
     * @return bool
     */
    protected function isPending($request)
    {
        $sms_verifcation = $this->sms_verifcation
                                ->getLatestVerification($request);

        $status = $sms_verifcation->status == 'pending' ? true : false;

        return $status;
    }

    /**
     * Check max numbers of attempts
     *
     * @param $request
     *
     * @return bool
     */
    protected function maxAttempts($request)
    {
        $entity = $this->sms_verifcation
                       ->getLatestVerification($request);

        $result = (bool)($entity->attempts >= $this->attempts);

        if($result) {
            $entity->updateStatus('expired');
        }

        return $result;
    }

    /**
     * Verify code availability
     *
     * @param $request
     *
     * @return bool
     */
    public function isAvailable($request)
    {
        $sms_verifcation = $this->sms_verifcation
                                ->getLatestVerification($request);

        $excludeEntityId = $sms_verifcation->id;
        $this->sms_verifcation->updateEntitiesStatus($excludeEntityId);

        $sms_verifcation->increment('attempts', 1);

        if($request->code == $sms_verifcation->code) {
            $sms_verifcation->updateStatus('confirmed');

            return true;
        }

        return false;
    }
}
