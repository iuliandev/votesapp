<?php

namespace App\Repositories\Sms;

use App\SmsVerification;
use Illuminate\Http\Request;

interface SmsRepositoryInterface
{
    public function create($request);
}
