<?php


function getError($error_code)
{
    $message = 'Something went wrong! Please try again or contact us to report it.';

    $errors = array(
        '21211' => 'This phone number is invalid.',
        '21612' => 'We cannot route to this number.',
        '21408' => 'We cannot route international numbers.',
        '21610' => 'This number is blacklisted. Please contact us.',
        '21614' => 'This number is incapable of receiving SMS messages.',
        'not_exist' => 'Make sure you\'re sent confirmation request.',
        'expired' => 'Your confirmation request has expired.',
        'max_attempts' => 'You have exceeded the maximum number of attempts.',
        'not_available' => 'Make sure you\'re using correct confirmation code.',
        'not_confirmed' => 'Please send confirmation request'
    );

    if(isset($errors[$error_code])) {
        $message = $errors[$error_code];
    }

    return $message;
}
