<?php

function jsonResponse($status, $code, $data)
{
    if(isset($data->collection)) {
       $data->additional([
           'status' => $status,
           'status_code' => $code
       ]);

       return $data;
    }

    if($status == 'error') {
       if($code == 500)
          $data = array('message' => $data);

       return response()->json([
           'status' => $status,
           'status_code' => $code,
           'errors' => $data
       ], $code);
    }

    return response()->json([
       'status' => $status,
       'status_code' => $code,
       'data' => $data
    ], $code);
}
