<?php

use Illuminate\Database\Seeder;

class OauthAccessTokensTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('oauth_access_tokens')->delete();
        
        \DB::table('oauth_access_tokens')->insert(array (
            0 => 
            array (
                'id' => '4887dc892c8364d9e97231d67a6f5f863e7aa6fbb6fcefa1e24b87a18ad74027ffa2725cf3aae0cb',
                'user_id' => 1,
                'client_id' => 1,
                'name' => 'AppName',
                'scopes' => '[]',
                'revoked' => 0,
                'created_at' => '2020-01-10 17:21:05',
                'updated_at' => '2020-01-10 17:21:05',
                'expires_at' => '2021-01-10 17:21:05',
            ),
            1 => 
            array (
                'id' => '53ff8d733b0a5d26ec44ce2bc732441b0d2f1eb2f2ef9c5e33fb251740c9a8db1b5f2fac7ad055a3',
                'user_id' => 5,
                'client_id' => 1,
                'name' => 'AppName',
                'scopes' => '[]',
                'revoked' => 0,
                'created_at' => '2019-12-06 15:38:24',
                'updated_at' => '2019-12-06 15:38:24',
                'expires_at' => '2020-12-06 15:38:24',
            ),
            2 => 
            array (
                'id' => '8fcaf667599863a4f3da34732d485269d0303f777433699d06faa4b1f407dac5638be3c5a1a1be5e',
                'user_id' => 5,
                'client_id' => 1,
                'name' => 'AppName',
                'scopes' => '[]',
                'revoked' => 0,
                'created_at' => '2019-12-06 15:40:01',
                'updated_at' => '2019-12-06 15:40:01',
                'expires_at' => '2020-12-06 15:40:01',
            ),
            3 => 
            array (
                'id' => 'b26ab688714abaed284eecf01f9f6c4d65a204c7dff33a1777779da911f82eb05c2bed7446d57926',
                'user_id' => 1,
                'client_id' => 1,
                'name' => 'AppName',
                'scopes' => '[]',
                'revoked' => 0,
                'created_at' => '2020-01-10 16:40:18',
                'updated_at' => '2020-01-10 16:40:18',
                'expires_at' => '2021-01-10 16:40:18',
            ),
            4 => 
            array (
                'id' => 'edf6f6eefbc77d3ec21734978c065d4752341b4c20968ae6fd093be282a279100d07d8ed32b33d8b',
                'user_id' => 1,
                'client_id' => 1,
                'name' => 'AppName',
                'scopes' => '[]',
                'revoked' => 0,
                'created_at' => '2020-01-11 17:49:56',
                'updated_at' => '2020-01-11 17:49:56',
                'expires_at' => '2021-01-11 17:49:56',
            ),
        ));
        
        
    }
}