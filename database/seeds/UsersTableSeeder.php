<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Ion Ionel',
                'email' => 'ion@gmail.com',
                'phone_number' => '+37368600001',
                'zipcode' => '85001',
                'city' => 'Phoenix AZ',
                'status' => 'Registered',
                'avatar' => 'default/avatar.png',
                'created_at' => '2020-01-10 16:40:18',
                'updated_at' => '2020-01-10 16:40:18',
            ),
            1 => 
            array (
                'id' => 10,
                'name' => 'Iulian',
                'email' => NULL,
                'phone_number' => '+37368657325',
                'zipcode' => NULL,
                'city' => NULL,
                'status' => 'Unregistered',
                'avatar' => 'default/avatar.png',
                'created_at' => '2020-01-12 09:27:20',
                'updated_at' => '2020-01-12 09:27:20',
            ),
            2 => 
            array (
                'id' => 11,
                'name' => 'John Smith',
                'email' => NULL,
                'phone_number' => '+37368655527',
                'zipcode' => NULL,
                'city' => NULL,
                'status' => 'Unregistered',
                'avatar' => 'default/avatar.png',
                'created_at' => '2020-01-12 09:27:20',
                'updated_at' => '2020-01-12 09:27:20',
            ),
            3 => 
            array (
                'id' => 12,
                'name' => 'Jora Turta',
                'email' => NULL,
                'phone_number' => '+37368655533',
                'zipcode' => NULL,
                'city' => NULL,
                'status' => 'Unregistered',
                'avatar' => 'default/avatar.png',
                'created_at' => '2020-01-12 09:27:20',
                'updated_at' => '2020-01-12 09:27:20',
            ),
            4 => 
            array (
                'id' => 13,
                'name' => 'Ion Bulbuc',
                'email' => NULL,
                'phone_number' => '+37368646324',
                'zipcode' => NULL,
                'city' => NULL,
                'status' => 'Unregistered',
                'avatar' => 'default/avatar.png',
                'created_at' => '2020-01-12 09:27:20',
                'updated_at' => '2020-01-12 09:27:20',
            ),
        ));
        
        
    }
}