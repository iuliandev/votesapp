<?php

use Illuminate\Database\Seeder;

class OauthClientsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('oauth_clients')->delete();
        
        \DB::table('oauth_clients')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => NULL,
                'name' => 'Votesapp Personal Access Client',
                'secret' => 'ug4SI0EAHCkUlYnaZWJaWXHdK0S8g2b6cezraO8T',
                'redirect' => 'http://localhost',
                'personal_access_client' => 1,
                'password_client' => 0,
                'revoked' => 0,
                'created_at' => '2019-12-06 15:34:13',
                'updated_at' => '2019-12-06 15:34:13',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => NULL,
                'name' => 'Votesapp Password Grant Client',
                'secret' => '5RQEVd3GrImvlikvMs8XDtrf0PLhketJL0y1NFy4',
                'redirect' => 'http://localhost',
                'personal_access_client' => 0,
                'password_client' => 1,
                'revoked' => 0,
                'created_at' => '2019-12-06 15:34:13',
                'updated_at' => '2019-12-06 15:34:13',
            ),
        ));
        
        
    }
}