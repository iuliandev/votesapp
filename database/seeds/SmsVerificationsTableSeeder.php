<?php

use Illuminate\Database\Seeder;

class SmsVerificationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sms_verifications')->delete();
        
        \DB::table('sms_verifications')->insert(array (
            0 => 
            array (
                'id' => 1,
                'phone_number' => '+37368600001',
                'code' => '51032',
                'attempts' => 1,
                'status' => 'expired',
                'created_at' => '2020-01-10 16:40:07',
                'updated_at' => '2020-01-11 17:49:48',
            ),
            1 => 
            array (
                'id' => 2,
                'phone_number' => '+37368600001',
                'code' => '49985',
                'attempts' => 1,
                'status' => 'confirmed',
                'created_at' => '2020-01-11 17:49:40',
                'updated_at' => '2020-01-11 17:49:48',
            ),
        ));
        
        
    }
}