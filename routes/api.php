<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function () {
    Route::post('send-sms',                 'SmsController@store');
    Route::post('verify-contact',           'SmsController@verifyContact');

    Route::group(['middleware' => 'is.sms.confirmed'], function() {
        Route::post('users/store',                 'UserController@store');
    });

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('user',                         'UserController@show');

        Route::get('contacts',                     'ContactsController@index');
        Route::post('contacts/sync',               'ContactsController@sync');
        Route::get('contacts/show/{id}',           'ContactsController@show');
        Route::post('contacts/destroy/{id}',       'ContactsController@destroy');
        Route::post('contacts/destroy-multiple',   'ContactsController@destroyMultiple');
    });

    Route::get('media/{entity}/{folder}/{file}',   'MediaController@index');
});
